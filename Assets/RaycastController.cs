﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaycastController : MonoBehaviour {


	public float maxDistanceRay=100f;
	public static RaycastController instance;
	public Text birdName;
	public Transform gunFlashTarget;
	public float fireRate=1.6f;
	private bool nextShot=true;
	private string objName="";

	AudioSource audio;
	public AudioClip[] clips;


	void Awake(){
		if(instance==null){
			instance=this;
		}
	}

	public void playSound(int sound){
		audio.clip= clips[sound];
		audio.Play();
	}

	public void Fire(){
		if(nextShot){
			StartCoroutine (takeshot());
			nextShot=false;
		}
	}

	private IEnumerator takeshot(){

		gunScript.instance.fireSound();

		Ray ray=Camera.main.ViewportPointToRay( new Vector3 (0.5f,0.5f,0));
		RaycastHit hit;

		int layer_mask= LayerMask.GetMask ("birdLayer");
		if(Physics.Raycast (ray,out hit, maxDistanceRay, layer_mask)){
			objName= hit.collider.gameObject.name;
			birdName.text=objName;

			Vector3 birdPosition= hit.collider.gameObject.transform.position;
			if(objName == "Bird_Assest (Clone)"){

			GameObject Blood= Instantiate (Resources.Load("blood", typeof (GameObject))) as GameObject;
		    Blood.transform.position= birdPosition;

				Destroy (hit.collider.gameObject);

				StartCoroutine (spawnNewBird());
				StartCoroutine (clearBlood());
				
			}
		}



		GameObject gunFlash= Instantiate (Resources.Load("gunFlashSmoke", typeof (GameObject))) as GameObject;
		gunFlash.transform.position= gunFlashTarget.position;


		yield return new WaitForSeconds (fireRate);

		nextShot= true;
	}


	// Use this for initialization
	void Start () {
		StartCoroutine (spawnNewBird());
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private IEnumerator clearBlood(){
		yield return new WaitForSeconds(1.5f);
		GameObject[] smokeGroup= GameObject.FindGameObjectsWithTag ("Blood");
		foreach(GameObject smoke in smokeGroup)
			Destroy(smoke.gameObject); 
	}




	private IEnumerator spawnNewBird(){
		yield return new WaitForSeconds(3f);

		//spawn new bird
		GameObject newBird=Instantiate (Resources.Load("Bird_Asset", typeof(GameObject))) as GameObject;

		//make the bird as a child of imagetarget

		newBird.transform.parent=GameObject.Find("Ground_Plane_Stage").transform;
		
		//scale bird size

		newBird.transform.localScale = new Vector3(2.2f,2.2f,2.2f);
	
		Vector3 temp;

		temp.x = Random.Range(15f, 55f);
        temp.y = Random.Range(15f, 25f);
        temp.z = Random.Range(15f, 55f);
        newBird.transform.position = new Vector3(temp.x, temp.y, temp.z);




	
	}

}
