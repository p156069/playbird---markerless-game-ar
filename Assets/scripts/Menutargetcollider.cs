﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menutargetcollider : MonoBehaviour {


	public static Menutargetcollider instance;

	void Awake(){
		if(instance == null){
			instance = this;
		}
	}

	void onTriggerEnter(Collider other){

		moveTarget();

	}

	public void moveTarget(){

		Vector3 temp; 
		temp.x = Random.Range(10f, 50f);
		temp.y = Random.Range(10f, 40f);
		temp.z = Random.Range(40f, 10f);
        transform.position = new Vector3 (temp.x, temp.y, temp.z);
		 

	}



}
