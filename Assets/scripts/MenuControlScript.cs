﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControlScript : MonoBehaviour {

	// Use this for initialization
	AudioSource audio;
	public AudioClip[] clips;

	void Start () {
		audio=GetComponent <AudioSource> ();
		StartCoroutine (introJingle());
	}

	private void playSound(int sound){

		audio.clip= clips [sound];
		audio.Play();
	}

	private IEnumerator introJingle(){
		yield return new WaitForSeconds (2f);
		playSound(0);
		StartCoroutine(quack());
	}



	private IEnumerator quack(){
		yield return new WaitForSeconds(2.2f);
		playSound(1);
		StartCoroutine (dog());
		
	}

	private IEnumerator dog(){
		yield return new WaitForSeconds (0.5f);
		playSound(2);
		StartCoroutine (gunshot());
	}


	private IEnumerator gunshot(){
		yield return new WaitForSeconds (0.8f);
		playSound(3);
	}
    
	public void ChangeScene(){
		SceneManager.LoadScene("Main_Scene");
	}
	
	 
}
